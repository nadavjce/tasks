
<head>

<style>
    h1 {
      color: white;
      text-align: center;
      text-decoration: underline;
    }
          table {
            margin-top: 50px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

          td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            
          }

          tr:nth-child(even) {
            background-color: #dddddd;
      
          }
          tr:nth-child(odd){
            background-color: #aaaaaa;
      
          }
          tr:nth-child(even):hover {
            background-color: gray;
      
          }
          tr:nth-child(odd):hover {
            background-color: gray;
      
          }
</style>
</head>

@extends('layouts.app')
@section('content')

<body>
          <table>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>status</th>
              <th>user_id</th>
              <th>edit</th>
            </tr>
                    



            @foreach($tasks as $task)
            <tr>
             <td>{{$task->id}}</td>
            <td> {{$task->title}}</td>  
             <td>{{$task->status}}</td>
             <td>{{$task->user_id}}</td>
             <td><a href = "{{route('tasks.edit' , $task->id) }}"> edit task </a></td>
             
            @endforeach
           </table>
<a href = "{{route('tasks.create') }}"> Create a new task </a>

</body>
   
@endsection