<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Task;
use App\User;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
        if (Auth::check()) {
            // The user is logged in...
        
        $id= Auth::id();
        $user = User::find($id);
        $tasks = $user->tasks;
        return view('tasks.index', compact('tasks'));
        }
        return redirect()->intended('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
        return view('tasks.create');
        }
        return redirect()->intended('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $tasks = new Task(); 
        $tasks->title = $request->title;
        $tasks->status = $request->status;
        $tasks->user_id = Auth::id();
        $tasks->created_at = null;
        $tasks->updated_at = null;
        $tasks->save();
        return redirect('tasks');
            }
            return redirect()->intended('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::check()) {
            return redirect()->intended('/tasks');
            }
            return redirect()->intended('/home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
             
            $task = Task::find($id);
                return view('tasks.edit', compact('task'));

            }
            return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            $task = Task::findOrFail($id);
       // לא עובד if(!$book->user->id == Auth::id()) return(redirect('books'));
        //test if title is dirty
        //$task->update($request->except(['_token']));
        //$task->title = update($request->title);
       // $task->status = update($request->status);
        
            return redirect('tasks');           
        }
            
            return redirect()->intended('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
            return redirect()->intended('/tasks');
            }
            return redirect()->intended('/home');
    }
    
}
